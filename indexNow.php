<?php

include_once 'config/database.php';

if(!array_key_exists('id_transaction', $_GET)){
    return;
}

define("TABLE_GAME", "2");

//Initialize db connection
$database = new Database();
$db = $database->getConnection();

$idTransaction = $_GET['id_transaction'];
$arrParam = ['idTransaction' => $idTransaction];

//Get client data
$sqlMsgInClient = "
        SELECT
            id_event_client,
            msg_in_client
        FROM
            sgad_italia.events_client ec
            JOIN sgad_italia.sessions s ON ec.id_session = s.id_session AND ec.id_platform = s.id_platform
            JOIN sgad_italia.games g on s.id_game = g.id_game
    
        WHERE
            ec.id_transaction_client = :idTransaction
            and id_action_client = 3
        group by
            id_event_client
         ";

$stmtClient = $db->prepare($sqlMsgInClient);
$stmtClient->execute($arrParam);

$rowsClient = $stmtClient->fetchAll();

//Get sss data
$sql = " 
        SELECT
            id_transaction,
            msg_out_sss,
            msg_in_client,
	       type_game,
            s.id_game,
            url_game,
            g.columns,
            g.rows
        FROM
            sgad_italia.events_sss es
            JOIN sgad_italia.events_client ec on es.id_transaction = substring_index(id_transaction_client,'|',2)
            JOIN sgad_italia.sessions s ON ec.id_session = s.id_session AND ec.id_platform = s.id_platform
            JOIN sgad_italia.games g on s.id_game = g.id_game
            
        WHERE 
            es.id_transaction = :idTransaction
            and id_action_client = 3 
        group by
            id_event_sss
         ";

$stmt = $db->prepare($sql);
$stmt->execute($arrParam);

$rows = $stmt->fetchAll();

if($rows == false){
    
    echo "Transazione non trovata!!!!!";
    return;
}
$typeGame = null;

foreach($rows as $row){
    
    $typeGame = $row['type_game'];
}

if($typeGame != TABLE_GAME){
    
    $msgOutSSS = json_decode($row['msg_out_sss']);
    //Recupero linee di vincita
    $lineeVincitaMsgOut = $msgOutSSS->Linee;
    $lineeVincita = [];
    $count = 1;
    foreach($lineeVincitaMsgOut as $linea){
        $l = $linea;
        $l->n = $count++;
        
        if(count($l->p) > 0){
            array_push($lineeVincita, $l);
        }
    }
    
    if(isset($msgOutSSS->FreeSpin)){
        
        $lineeVincitaMsgOutFs = $msgOutSSS->FreeSpinWinMap;
        
        $lineeVincitaMsgOutFs->n = "-";
        
        if(count($lineeVincitaMsgOutFs->p) > 0){
            array_push($lineeVincita, $lineeVincitaMsgOutFs);
        }
    }

}

//Funzione per la creazione della combinazione uscita
function getImageCombination($transaction, $jsonCombination,$symbolGameWidth, $symbolGameHeight, $par=null){
    /**
     * Recupero delle immagini dei simboli del gioco
     */
    print_r($par);
    $url = $transaction['url_game']."resources/Symbol/symbolON";
    $stringSymbols = file_get_contents($url.".json");

    $dataSymbols = json_decode($stringSymbols, true);
    
    
    $src = imagecreatefrompng($url.".png");

    $arrSymbolImage = [];
    
    if($transaction['type_game'] == TABLE_GAME){
        
        $imageHold = null;
        
        $urlHold = $transaction['url_game']."resources/Symbol/hold.png";

        
        $img = $dataSymbols['frames'];
        
        if(file_get_contents($urlHold) !== false){
            $dataHold = imagecreatefromstring(file_get_contents($urlHold));
            $sizeHold = getimagesize($urlHold);
            $holdWidth = $sizeHold[0];
            $holdHeight = $sizeHold[1];
            
            $dest = imagecreatetruecolor($holdWidth, $holdHeight);
            $black = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $black);
            
            imagecopy($dest, $dataHold, 0, 0, 0, 0, $holdWidth, $holdHeight);
            $imageHold = $dest;
        }
        
        foreach($img as $imKey=>$imValue)
        {
            
            $srcX = $imValue['frame']['x'];
            $srcY = $imValue['frame']['y'];
            $srcWidth = $imValue['frame']['w'];
            $srcHeight = $imValue['frame']['h'];
            
            $dest = imagecreatetruecolor($srcWidth, $srcHeight);
            imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);

            $arrSymbolImage[$imKey] = $dest;
            
        }
    }
    else{
        $nImage = count($dataSymbols['frames']);
        /**
         * Ottengo un'array contenente i sinboli del gioco
         */
        for($i = 1; $i <= $nImage; $i++)
        {
            $index = str_pad($i, 2, "0", STR_PAD_LEFT);
            
            $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
            $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
            $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
            $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
            
            $dest = imagecreatetruecolor($srcWidth, $srcHeight);
            
            imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
            array_push($arrSymbolImage, $dest);
            
        }
    }
    /**
     * Recupero le righe e le colonne del gioco
     */
    
    $rows = $transaction['rows'];
    $columns = $transaction['columns'];
    
    
    $arrSymbols = [];
    $nColumn = 0;
    $nRow = 0;
    /**
     * Creo l'immagine vuota con altezza e larghezza che servono
     */
     $eventSymbolImage = imagecreatetruecolor($symbolGameWidth * $columns, $symbolGameHeight * $rows);
     
    /**
     * Ciclo sui simboli della combinazione
     */
    for($i = 0; $i < count($jsonCombination); $i++)
    {
        $symbol = $jsonCombination[$i];
        /**
         * Recupero l'immagine relativa al simbolo
         */
        $tmpSymbol = 0;
        if($transaction['type_game'] == TABLE_GAME){
            
            $tmpSymbol = $symbol.".png";
            $imageSymbol = $arrSymbolImage[$tmpSymbol];
            $imageSymbolWidth = imagesx($imageSymbol);
            $imageSymbolHeight = imagesy($imageSymbol);
            if($par != null){
                for($y = 0; $y< sizeof($par); $y++){
                    if($i == $par[$y]){
                        $holdWidth = imagesx($imageHold);
                        $holdHeight = imagesy($imageHold);
                        imagecopy($imageSymbol, $imageHold, ($imageSymbolWidth/2) - ($holdWidth/2), $imageSymbolHeight - $holdHeight, 0, 0, $holdWidth, $holdHeight);
                    }
                }
            }
            
            
            
        }
        else{
            $tmpSymbol = $symbol-1;
            $imageSymbol = $arrSymbolImage[$tmpSymbol];
            $imageSymbolWidth = imagesx($imageSymbol);
            $imageSymbolHeight = imagesy($imageSymbol);
        }
        
        
        
        /**
         * Calcolo la colonna e la riga del simbolo
         */
        if($i != 0)
        {
            if($i % $columns == 0)
            {
                $nColumn = 0;
                $nRow++;
            }
            else{
                
                $nColumn++;
            }
        }
        /**
         * Copio l'immagine nella posizione calcolata
         */
         imagecopyresized($eventSymbolImage, $imageSymbol, $symbolGameWidth * $nColumn, $symbolGameHeight * $nRow, 0, 0, $symbolGameWidth, $symbolGameHeight, $imageSymbolWidth, $imageSymbolHeight);
         array_push($arrSymbols, 1);
    }
    
    
    ob_start();
    imagepng($eventSymbolImage);
    $contents = ob_get_contents();
    ob_end_clean();
    
    
    return base64_encode($contents);
}


?>
<!DOCTYPE html>
<html>
    <head>
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/skin_color.css">
		<link rel="stylesheet" href="assets/css/vendors_css.css">
        <script src="assets/js/vendors.min.js"></script>
        <script type="text/javascript">
        	function SeeWinLine(url, rows, columns, line, combination){
        		$.ajax({
                        type: 'GET',
                        url: 'getWinLine.php',
                        async: true,
                        data: {url:url, rows:rows, columns:columns, line:line, combination:combination},
                        success: function(data){
                        	var imageNew = data; 
                            document.getElementById("imageConf").src="data:image/png;base64,"+imageNew;
                        },
                        error: function(xhr){
                            $('#resultCreate').append('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                         },
                        complete: function(data){
                            $('#resultCreate').append(data);
                        }
                    });
        	}
        </script>
    </head>
    <body class="dark-skin theme-primary">
    	
        <div class="wrapper">
        	<div class="content-wrapper">
        		<div class="row">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-body">
                                <div style="display: block;height: auto;">
                                	<?php   
                                	for($i = 0; $i< sizeof($rows); $i++){
                                	    $row = $rows[$i];
                                	    $msgOutSSS = json_decode($row['msg_out_sss']);
                                	    
                                	    if($typeGame == TABLE_GAME){
                                	        
                                	        if($i == 0){
                                	            
                                	            foreach($rowsClient as $rowClient){
                                	                $msgInClient = explode(";",$rowClient['msg_in_client']);
                                	                $parEl = explode("=",$msgInClient[count($msgInClient) - 2]);
                                	                
                                	                $parValue = $parEl[1];
                                	                
                                	                if($parValue != ""){
                                	                    $par = explode(",", $parValue);
                                	                    
                                	                }
                                	            }
                                	            
                                	        }
                                	        else{
                                	            $par = null;
                                	        }
                                	        
                                	        
                                	       
                                	        
                                    	    $combination = $msgOutSSS->config;
                                    	    $image = getImageCombination($row,$combination, 200,254, $par);
                                    	    ?>
                                    	    <div class="row">
                                            	<div class="col-md-12">
                                                	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
        
                                                </div>
                                                
                                    	
                                    		</div>
                                    	    <?php 
                                    	    
                                    	}
                                    	else{
                                    	    $combination = $msgOutSSS->Combination;
                                    	    $image = getImageCombination($row,$combination, 128,128);
                                    	    
                                    	    ?>
                                    	    <div class="row">
                                    	<div class="col-md-8">
                                        	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">

                                        </div>
                                        <div class="col-md-4">
                                        	<table id="session-detail-table" class="table table-lg">
                                                <thead>
                                                    <tr>
                                                    	<th>Linea vincente</th>
                                                    	<th>Simboli vincenti sulla linea</th>
                                                        <th>Simbolo vincente</th>
                                                    </tr>
                                                </thead>
                                    			<tbody>
                                        		<?php
                                        		
                                        		foreach($lineeVincita as $lv){
                                        		    $linee = $lv->p;
                                        		    $strLinee = "";
                                        		    foreach($linee as $l){
                                        		        $pos = intval($l)+1;
                                        		        $strLinee .= $pos.",";
                                        		    }
                                        		    $strLinee = substr($strLinee, 0, strlen($strLinee)-1);
                                        		    $simbol = $lv->s;
                                        		    $nLineaVincita = $lv->n;
                                                ?>
                                                    <tr>
                                                    	<td><?=$nLineaVincita?></td>
                                                    	<td onclick='SeeWinLine(<?php echo json_encode($row["url_game"]); ?>,<?php echo json_encode($row["rows"]); ?>, <?php echo json_encode($row["columns"]); ?>,<?php echo json_encode($linee);?>, <?php  echo json_encode($combination);?>)'><?=$strLinee?></td>
                                                        <td><?=$simbol?></td>
                                                    </tr>
                                        		<?php 
                                        		}
                                        		?>
                                    			</tbody>
                            				</table>

                                    	</div>
                                    	
                                    </div>
                                    	    <?php 
                                    	}
                                        
                                    ?>

                                	
                                    <?php 
                                	}
                                    ?>
                                </div>
                                
                            </div>
                        </div>
                </div>
            	<div class="col-sm-12 col-md-12">
                	<div class="box">
                        <div class="box-body">
                            <div class="table-responsive">
                            <?php 
                            if($typeGame == TABLE_GAME){
                                ?>
                                <table id="session-detail-table" class="table table-lg">
                                    <thead>
                                        <tr>
                                            <th>Transazione</th>
                                            <th>Bet</th>
                                            <th>Vincita Totale</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                <?php
                                $transaction = "";
                                foreach($rows as $row){
                                    
                                    $msgOutSSS = json_decode($row['msg_out_sss']);
                                    
                                    $transaction .= $row['id_transaction']." - ";
                                    $vincitaTotale = 0;
                                    
                                    if($msgOutSSS->VincitaTotale == 0){
                                        $msgInClient = explode(";",$row['msg_in_client']);
                                        $betArr = explode("=",$msgInClient[1]);
                                        $bet = $betArr[1];
                                    }
                                    else{
                                        
                                        $vincitaTotale = $msgOutSSS->VincitaTotale;
                                    }
                                    
                                }
                                $transaction = substr($transaction, 0, strlen($transaction) - 3);
                                    ?>
                                        <tr>
                                        	<td><?=$transaction?></td>
                                            <td><?=$bet?></td>
                                            <td><?=$vincitaTotale?></td>
                                        </tr>
                                    </tbody>
                            	</table>
                            <?php 
                            }
                            else{
                                ?>
                                <table id="session-detail-table" class="table table-lg">
                                    <thead>
                                        <tr>
                                        	<th>Transazione</th>
                                            <th>Bet</th>
                                            <th>Vincita Totale</th>
                                            <th>Vincita Linee</th>
                                            <th>Vincita Freespin</th>
                                            <th>Vincita Bonus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php   
                                        foreach($rows as $row){
                                            $transaction = $row['id_transaction'];
                                            
                                            
                                            $vincitaTotale = $msgOutSSS->VincitaTotale;
                                            $vincitaLinee = $msgOutSSS->VincitaLinee;
                                            $vincitaScatter = $msgOutSSS->VincitaScatter;
                                            $vincitaBonus = $msgOutSSS->VincitaBonus;
                                            
                                            $msgInClient = explode(";",$row['msg_in_client']);
                                            $betArr = explode("=",$msgInClient[1]);
                                            $bet = $betArr[1];

                                        ?>
                                            <tr>
                                            	<td><?=$transaction?></td>
                                                <td><?=$bet?></td>
                                                <td><?=$vincitaTotale?></td>
                                                <td><?=$vincitaLinee?></td>
                                                <td><?=$vincitaScatter?></td>
                                                <td><?=$vincitaBonus?></td>
                                            </tr>
                                      <?php 
                                        }
                                      ?>
                                    </tbody>
                            	</table>
                                
                                
                                <?php 
                                
                            }
                            ?>
                                
                            </div>
                        </div>
                    </div>
                    
                	<?php 
                	if(isset($msgOutSSS->BonusArray)){
                	?>
                       <div class="box">
                            <div class="box-body">
                                <div class="table-responsive">
                            		<table id="session-detail-table" class="table table-lg">
                                        <thead>
                                            <tr>
                                            	<th>Vincita bonus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php   
                                            
                                            foreach($msgOutSSS->BonusArray as $bp){
                                                
                                                $win = $bp;
                                            ?>
                                                <tr>
                                                	<td><?=$win?></td>
                                                </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                            		</table>
                         		</div>
                			</div>
                 		</div>	
                	<?php 
                	    
                	}
                	?>
                     
                    <?php 
                	if(isset($msgOutSSS->FreeSpin)){
                	?>
                       <div class="box">
                            <div class="box-body">
                                <div class="table-responsive">
                            		<table id="session-detail-table" class="table table-lg">
                                        <thead>
                                            <tr>
                                            	<th>Combinazione freespin</th>
                                            	<th>Vincita totale freespin</th>
                                                <th>Linee vincita freespin</th>
                                                <th>Vincita linee freespin</th>
                                                <th>Simbolo linee freespin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php   
                                            
                                            foreach($msgOutSSS->FreeSpin as $fs){
                                                
                                                $win = $fs->Win;
                                                $combination = $fs->Combination;
                                                $imageFs = getImageCombination($row,$combination, 64,64);
                                                
                                                $winLines = $fs->WinLines;
                                                $rowSpan = count($winLines);
                                                if($rowSpan == 0){
                                                    $rowSpan = 1;
                                                }
                                                
                                            ?>
                                                <tr>
                                                	<td rowspan="<?=$rowSpan?>">
                                                		<img src="data:image/png;base64,<?=$imageFs?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                	</td>
                                                	<td rowspan="<?=$rowSpan?>"><?=$win?></td>	
                                                        <?php 
                                                        if($win > 0){
                                                            $i= 0;
                                                            foreach($winLines as $lines){
                                                                $linee = $lines->p;
                                                                $strLinee = "";
                                                                foreach($linee as $l){
                                                                    $pos = intval($l)+1;
                                                                    $strLinee .= $pos.",";
                                                                }
                                                                $strLinee = substr($strLinee, 0, strlen($strLinee)-1);
                                                                
                                                                $winLinee = $lines->w;
                                                                $sLinee = $lines->s;
                                                                if($i > 0){
                                                                    ?>
                                                             	<tr>  
                                                            <?php 
                                                              
                                                            }
                                                            ?>
                                                            	<td><?=$strLinee?></td>
                                                            	<td><?=$winLinee?></td>
                                                            	<td><?=$sLinee?></td>
                                                       		<?php 
                                                       	    if($i > 0){
                                                       	    ?>
                                                             </tr>  
                                                            <?php 
                                                              
                                                            }
                                                            $i++;
                                                        }
                                                      }
                                                      else{
                                                          ?>
                                                        	<td>-</td>
                                                        	<td>-</td>
                                                        	<td>-</td>
                                                        <?php 
                                                      }
                                                       	?>
                                                 </tr>   

                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                            		</table>
                         		</div>
                			</div>
                 		</div>	
                	<?php 
                	    
                	}
                	?>       
                </div>
             </div>    
    	</div>
    	</div>
    </body>
</html>