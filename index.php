<?php

include_once 'config/database.php';

if(!array_key_exists('id_transaction', $_GET)){
    return;
}

define("SLOT_GAME_PLATFORM", "1");
define("TABLE_GAME_PLATFORM_OLD", "2");
define("TABLE_GAME_PLATFORM", "3");
define("TABLE_GAME_PLATFORM_INSTANT", "4");

//Initialize db connection
$database = new Database();
$db = $database->getConnection();

$idTransaction = $_GET['id_transaction'];
$arrParam = ['idTransaction' => $idTransaction];

//Get client data
$sqlMsgInClient = "
        SELECT
            id_event_client,
            msg_in_client,
            msg_out_client,
            ec.id_platform,
            ec.id_transaction_client,
            g.id_game,
            url_game,
            g.rows,
            g.columns
        FROM
            ".$database->db_name.".events_client ec
            JOIN ".$database->db_name.".sessions s ON ec.id_session = s.id_session AND ec.id_platform = s.id_platform
            JOIN ".$database->db_name.".games g on s.id_game = g.id_game
    
        WHERE
            ec.id_transaction_client = :idTransaction
            and (id_action_client = 3 or id_action_client = 5)
        group by
            id_event_client
        order by 
            id_event_client
         ";

$stmtClient = $db->prepare($sqlMsgInClient);
$stmtClient->execute($arrParam);

$rowsClient = $stmtClient->fetchAll();
$idPlatform = $rowsClient[0]['id_platform'];

//Get sss data
$sql = " 
        SELECT
            id_transaction,
            msg_out_sss,
            msg_in_client,
	        type_game,
            s.id_game,
            url_game,
            g.columns,
            g.rows,
            ec.id_platform
        FROM
            ".$database->db_name.".events_sss es
            JOIN ".$database->db_name.".events_client ec on es.id_transaction = substring_index(id_transaction_client,'|',2)
            JOIN ".$database->db_name.".sessions s ON ec.id_session = s.id_session AND ec.id_platform = s.id_platform
            JOIN ".$database->db_name.".games g on s.id_game = g.id_game
            
        WHERE 
            es.id_transaction = :idTransaction
            and id_action_client = 3 
        group by
            id_event_sss
         ";

$stmt = $db->prepare($sql);
$stmt->execute($arrParam);

$rows = $stmt->fetchAll();

if($rows == false){
    
    echo "Transazione non trovata!!!!!";
    return;
}




//Se � una slot
if($idPlatform == SLOT_GAME_PLATFORM || $idPlatform == TABLE_GAME_PLATFORM_INSTANT){
    
    $typeGame = null;
    foreach($rows as $row){
    
         $typeGame = $row['type_game'];
    }
    
    $msgOutSSS = json_decode($row['msg_out_sss']);
    //Recupero linee di vincita
    $lineeVincitaMsgOut = $msgOutSSS->Linee;
    $lineeVincita = [];
    $count = 1;
    foreach($lineeVincitaMsgOut as $linea){
        $l = $linea;
        $l->n = $count++;
        
        if(count($l->p) > 0){
            array_push($lineeVincita, $l);
        }
    }
    
    if(isset($msgOutSSS->FreeSpin)){
        
        $lineeVincitaMsgOutFs = $msgOutSSS->FreeSpinWinMap;
        
        $lineeVincitaMsgOutFs->n = "-";
        
        if(count($lineeVincitaMsgOutFs->p) > 0){
            array_push($lineeVincita, $lineeVincitaMsgOutFs);
        }
    }

}

//Funzione per la creazione della combinazione uscita
function getImageCombination($transaction, $jsonCombination,$symbolGameWidth, $symbolGameHeight, $arrSymbolImage, $par=null){

    
    if($transaction['id_platform'] == TABLE_GAME_PLATFORM_OLD){
        
        $imageHold = null;
        
        $urlHold = $transaction['url_game']."resources/Symbol/hold.png";
        
        if(file_get_contents($urlHold) !== false){
            $dataHold = imagecreatefromstring(file_get_contents($urlHold));
            $sizeHold = getimagesize($urlHold);
            $holdWidth = $sizeHold[0];
            $holdHeight = $sizeHold[1];
            
            $dest = imagecreatetruecolor($holdWidth, $holdHeight);
            $black = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $black);
            
            imagecopy($dest, $dataHold, 0, 0, 0, 0, $holdWidth, $holdHeight);
            $imageHold = $dest;
        }

    }

    /**
     * Recupero le righe e le colonne del gioco
     */
    
    $rows = $transaction['rows'];
    $columns = $transaction['columns'];
    
    
    $arrSymbols = [];
    $nColumn = 0;
    $nRow = 0;
    /**
     * Creo l'immagine vuota con altezza e larghezza che servono
     */

    if($transaction['id_platform'] == TABLE_GAME_PLATFORM){
        
        $eventSymbolImage = imagecreatetruecolor($symbolGameWidth * count($jsonCombination), $symbolGameHeight);
        $columns = count($jsonCombination);
        
    }
    else{
        $eventSymbolImage = imagecreatetruecolor($symbolGameWidth * $columns, $symbolGameHeight * $rows);
    }
     
     
    /**
     * Ciclo sui simboli della combinazione
     */
    for($i = 0; $i < count($jsonCombination); $i++)
    {
        $symbol = $jsonCombination[$i];

        /**
         * Recupero l'immagine relativa al simbolo
         */
        $tmpSymbol = 0;
        if($transaction['id_platform'] == TABLE_GAME_PLATFORM_OLD){
            
            $tmpSymbol = $symbol.".png";
            $imageSymbol = $arrSymbolImage[$tmpSymbol];
            $imageSymbolWidth = imagesx($imageSymbol);
            $imageSymbolHeight = imagesy($imageSymbol);
            if($par != null){
                
                for($y = 0; $y< sizeof($par); $y++){
                    if($i == $par[$y]){
                        $holdWidth = imagesx($imageHold);
                        $holdHeight = imagesy($imageHold);
                        imagecopy($imageSymbol, $imageHold, ($imageSymbolWidth/2) - ($holdWidth/2), $imageSymbolHeight - $holdHeight, 0, 0, $holdWidth, $holdHeight);
                    }
                }
            }
            
            
            
        }
        else if($transaction['id_platform'] == TABLE_GAME_PLATFORM){
            
            $number = substr($symbol, 0, strlen($symbol)-1);

            $numberStr = "";
            switch($number){
                case "K":
                    $numberStr = "13";
                    break;
                case "Q":
                    $numberStr = "12";
                    break;
                case "J":
                    $numberStr = "11";
                    break;
                default:
                    $numberStr = $number;
                    break;
            }
            
            $seed = substr($symbol, strlen($symbol)-1, strlen($symbol));

            $seedStr = "";
            switch($seed){
                case "C":
                    $seedStr = "1";
                    break;
                case "Q":
                    $seedStr = "2";
                    break;
                case "F":
                    $seedStr = "3";
                    break;
                case "P":
                    $seedStr = "4";
                    break;
            }
            $realSymbol = "ccd_".$seedStr."x".$numberStr;

            $tmpSymbol = $realSymbol.".png";
            $imageSymbol = $arrSymbolImage[$tmpSymbol];
            $imageSymbolWidth = imagesx($imageSymbol);
            $imageSymbolHeight = imagesy($imageSymbol);
            
        }
        else{
            $tmpSymbol = $symbol-1;
            $imageSymbol = $arrSymbolImage[$tmpSymbol];
            $imageSymbolWidth = imagesx($imageSymbol);
            $imageSymbolHeight = imagesy($imageSymbol);
        }
        
        
        
        /**
         * Calcolo la colonna e la riga del simbolo
         */
        if($i != 0)
        {
            if($i % $columns == 0)
            {
                $nColumn = 0;
                $nRow++;
            }
            else{
                
                $nColumn++;
            }
        }
        /**
         * Copio l'immagine nella posizione calcolata
         */
         imagecopyresized($eventSymbolImage, $imageSymbol, $symbolGameWidth * $nColumn, $symbolGameHeight * $nRow, 0, 0, $symbolGameWidth, $symbolGameHeight, $imageSymbolWidth, $imageSymbolHeight);
         array_push($arrSymbols, 1);
    }
    
    
    ob_start();
    imagepng($eventSymbolImage);
    $contents = ob_get_contents();
    ob_end_clean();
    
    
    return base64_encode($contents);
}

function getImageCombinationMystery($transaction, $jsonCombination,$symbolGameWidth, $symbolGameHeight, $arrSymbolImage, $backgroundImage, $destUpDoor, $destSubDoor, $font){
    
    
    //$fontLoaded = imageloadfont("FontKeyboard.ttf");
    
    $arrSymbols = [];
    /**
     * Creo l'immagine vuota con altezza e larghezza che servono
     */
    
    $eventSymbolImage = imagecreatetruecolor(1920, 1080);
    
    imagecopyresized($eventSymbolImage, $backgroundImage, 0, 0, 0, 0, 1920, 1080, 1920, 1080);
    
    //Carico le tre porte superiori
    imagecopyresized($eventSymbolImage, $destUpDoor, 405, 300, 0, 0, 256, 256, 256, 256);
    imagecopyresized($eventSymbolImage, $destUpDoor, 807, 300, 0, 0, 256, 256, 256, 256);
    imagecopyresized($eventSymbolImage, $destUpDoor, 1214, 300, 0, 0, 256, 256, 256, 256);
    
    //Carico le porte inferiori
    imagecopyresized($eventSymbolImage, $destSubDoor, 70, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 265, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 452, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 640, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 836, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 1032, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 1228, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 1416, 630, 0, 0, 206, 256, 206, 256);
    imagecopyresized($eventSymbolImage, $destSubDoor, 1626, 630, 0, 0, 206, 256, 206, 256);
    

    $x = 0;
    $arrSymbolWin = [];
    /**
     * Ciclo sui simboli della combinazione delle porte sopra
     */
    for($i = 0; $i < 9; $i++)
    {
        $symbol = $jsonCombination->DoorDown[$i];
        
        /**
         * Recupero l'immagine relativa al simbolo
         */
        
        $imageSymbol = $arrSymbolImage[$symbol];
        $imageSymbolWidth = imagesx($imageSymbol);
        $imageSymbolHeight = imagesy($imageSymbol);
        
        $isWin = false;
        for($r = 0; $r < count($jsonCombination->WinnerIndex); $r++){
            if($i == $jsonCombination->WinnerIndex[$r]){
                $isWin = true;
                array_push($arrSymbolWin, $symbol);
            }
        }
        if($isWin == false){
            imagefilter($imageSymbol, IMG_FILTER_GRAYSCALE, 150);
        }
        
        switch($i){
            case 0:
                $x = 100;
                break;
            case 1:
                $x = 295;
                break;
            case 2:
                $x = 482;
                break;
            case 3:
                $x = 665;
                break;
            case 4:
                $x = 866;
                break;
            case 5:
                $x = 1062;
                break;
            case 6:
                $x = 1258;
                break;
            case 7:
                $x = 1446;
                break;
            case 8:
                $x = 1656;
                break;
        }
        
        /**
         * Copio l'immagine nella posizione calcolata
         */
        imagecopyresized($eventSymbolImage, $imageSymbol, $x, 700, 0, 0, $symbolGameWidth, $symbolGameHeight, $imageSymbolWidth, $imageSymbolHeight);
        array_push($arrSymbols, 1);
        
        $text = "� ".$jsonCombination->ValueWin[$i];
        $white = imagecolorallocate($eventSymbolImage, 255, 255, 255);
        
        imagestring($eventSymbolImage, 5, $x + 40,860, $text, $white);
    }
    
    /**
     * Ciclo sui simboli della combinazione delle porte sopra
     */
    
    for($i = 0; $i < 3; $i++)
    {
        $symbol = $jsonCombination->DoorUp[$i];
        
        /**
         * Recupero l'immagine relativa al simbolo
         */
        $imageSymbol = $arrSymbolImage[$symbol];
        $imageSymbolWidth = imagesx($imageSymbol);
        $imageSymbolHeight = imagesy($imageSymbol);
        
        /**
         * Controllo se ci sono delle vincite
         */
        $isWin = false;
        for($r = 0; $r < count($arrSymbolWin); $r++){
            
            if($symbol == $arrSymbolWin[$r]){
                $isWin = true;
            }
        }
        
        if($isWin == false){
            imagefilter($imageSymbol, IMG_FILTER_GRAYSCALE, 150);
        }
        
        switch($i){
            case 0:
                $x = 460;
                break;
            case 1:
                $x = 862;
                break;
            case 2:
                $x = 1269;
                break;
        }
        
        /**
         * Copio l'immagine nella posizione calcolata
         */
        imagecopyresized($eventSymbolImage, $imageSymbol, $x, 385, 0, 0, $symbolGameWidth, $symbolGameHeight, $imageSymbolWidth, $imageSymbolHeight);
        array_push($arrSymbols, 1);
    }
    
    imagesavealpha($eventSymbolImage, true);
    
    ob_start();
    imagepng($eventSymbolImage);
    $contents = ob_get_contents();
    ob_end_clean();
    
    
    return base64_encode($contents);
}

function getImageCombinationScratch($transaction, $jsonCombination,$symbolGameWidth, $symbolGameHeight, $arrSymbolImage, $rows, $columns){
    
    
    
    $arrSymbols = [];
    $nColumn = 0;
    $nRow = 0;
    /**
     * Creo l'immagine vuota con altezza e larghezza che servono
     */
    
   
    $eventSymbolImage = imagecreatetruecolor($symbolGameWidth * $columns, $symbolGameHeight * $rows);
    imagesavealpha($eventSymbolImage, true);
    imagefill($eventSymbolImage,0,0,0x7fff0000);
    /**
     * Ciclo sui simboli della combinazione
     */
    for($i = 0; $i < count($jsonCombination); $i++)
    {
        $symbol = $jsonCombination[$i];
        
        /**
         * Recupero l'immagine relativa al simbolo
         */
        $imageSymbol = $arrSymbolImage[$symbol - 1];
        $imageSymbolWidth = imagesx($imageSymbol);
        $imageSymbolHeight = imagesy($imageSymbol);
        
        
        
        /**
         * Calcolo la colonna e la riga del simbolo
         */
        if($i != 0)
        {
            if($i % $columns == 0)
            {
                $nColumn = 0;
                $nRow++;
            }
            else{
                
                $nColumn++;
            }
        }
        /**
         * Copio l'immagine nella posizione calcolata
         */
        imagecopyresized($eventSymbolImage, $imageSymbol, $symbolGameWidth * $nColumn, $symbolGameHeight * $nRow, 0, 0, $symbolGameWidth, $symbolGameHeight, $imageSymbolWidth, $imageSymbolHeight);
        array_push($arrSymbols, 1);
    }
    
    
    ob_start();
    imagepng($eventSymbolImage);
    $contents = ob_get_contents();
    ob_end_clean();
    
    
    return base64_encode($contents);
}

function GetSymbolImage($arrSymbolImage, $index, $isGray){
    
    $image = imagecreatetruecolor(41, 44);
    
    imagesavealpha($image, true);
    imagefill($image,0,0,0x7fff0000);
    $imageSymbol = $arrSymbolImage[$index];
    if($isGray == true){
        imagefilter($imageSymbol, IMG_FILTER_GRAYSCALE, 150);
    }
    $imageSymbolWidth = imagesx($imageSymbol);
    $imageSymbolHeight = imagesy($imageSymbol);
    imagecopyresized($image, $imageSymbol, 0, 0, 0, 0, 41, 44, $imageSymbolWidth, $imageSymbolHeight);
    ob_start();
    imagepng($image);
    $contents = ob_get_contents();
    ob_end_clean();
    return base64_encode($contents);
}

?>
<!DOCTYPE html>
<html>
    <head>
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/skin_color.css">
		<link rel="stylesheet" href="assets/css/vendors_css.css">
        <script src="assets/js/vendors.min.js"></script>
        <script type="text/javascript">
        	function SeeWinLine(url, rows, columns, line, combination){
        		$.ajax({
                        type: 'GET',
                        url: 'getWinLine.php',
                        async: true,
                        data: {url:url, rows:rows, columns:columns, line:line, combination:combination},
                        success: function(data){
                        	var imageNew = data; 
                            document.getElementById("imageConf").src="data:image/png;base64,"+imageNew;
                        },
                        error: function(xhr){
                            $('#resultCreate').append('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                         },
                        complete: function(data){
                            $('#resultCreate').append(data);
                        }
                    });
        	}
        </script>
    </head>
    <body class="dark-skin theme-primary">
        <div class="wrapper">
        	<div class="content-wrapper">
        	<?php 
        	switch($idPlatform){
        	    case TABLE_GAME_PLATFORM:
        	        ?>
        	        <div class="col-sm-12 col-md-12">
        	        	<div class="box">
    	        			<div class="box-body">
        	        			<div class="table-responsive">
        	        				<?php 
        	        					$arrAllPlayer = [];
        	        					
        	        					//Cerco tutti i giocatori
        	        					if($rowsClient[0]['msg_out_client'] != null){
        	        					      
            	        					$msgToSpit = explode(";", $rowsClient[0]['msg_out_client']);
            	        					$msgOutDataToSpit = explode("=", $msgToSpit[1]);
            	        					$msgOutClient = json_decode($msgOutDataToSpit[1], true);
            	        					
            	        					foreach($msgOutClient as $key=>$value){
            	        					    if($key != "GlobalWins"){
            	        					        array_push($arrAllPlayer,$key);
            	        					    }
            	        					}
        	        					}

        	        					$arrAllDataPlayer = [];
        	        					$nFase = 0;
        	        					//Scorro tutte le smazzate e creo la struttura
        	        					foreach($arrAllPlayer as $player){
        	        					    $y = 0;
    	        					        $arrAllDataPlayer[$player]["WinMain"] = 0;
    	        					        $arrAllDataPlayer[$player]["WinC213"] = 0;
    	        					        $arrAllDataPlayer[$player]["WinPair"] = 0;
        	        					    foreach($rowsClient as $row){
        	        					        
        	        					        //Parsifico il msg_in_client
        	        					        $msgToSpit = explode(";", $row['msg_in_client']);
        	        					        $msgInDataToSpit = explode("=", $msgToSpit[1]);
        	        					        
        	        					        if($row['msg_out_client'] != null){

        	        					            $jsonIn = json_decode($msgInDataToSpit[1], true);
        	        					            if(is_array($jsonIn)){
            	        					            if(array_key_exists("P",$jsonIn)){
            	        					                $numberPlayer = $jsonIn["P"];
            	        					            }
            	        					            if(array_key_exists("D",$jsonIn)){
            	        					                $isDouble = $jsonIn["D"];
            	        					                if($isDouble == 1){
            	        					                    if("P0".$numberPlayer == $player){
            	        					                        $arrAllDataPlayer["P0".$numberPlayer]["SpecialEvents".$y]  = "Double";
            	        					                        
            	        					                    }
            	        					                }
            	        					                
            	        					            }
        	        					            }
        	        					            $msgToSpit = explode(";", $row['msg_out_client']);
        	        					            $msgOutDataToSpit = explode("=", $msgToSpit[1]);
        	        					            $msgOutClient = json_decode($msgOutDataToSpit[1], true);
        	        					            
        	        					            $strDataPlayer = $msgOutClient[$player];

        	        					            if(array_key_exists("Point", $strDataPlayer)){
    	        					                    $arrAllDataPlayer[$player]["Point".$y]  = $strDataPlayer["Point"];
        	        					            }
        	        					            
        	        					            
        	        					            if(array_key_exists("Split", $strDataPlayer)){
        	        					                $arrAllDataPlayer[$player]["Split".$y]  = $strDataPlayer["Split"];
        	        					            }
        	        					            
        	        					            if(array_key_exists("Break", $strDataPlayer)){
        	        					                if($strDataPlayer["Break"] == 1){
        	        					                    $arrAllDataPlayer[$player]["SpecialEvents".$y]  = "Over";
        	        					                }
        	        					            }
        	        					            
        	        					            if(array_key_exists("BlackJack", $strDataPlayer)){
        	        					                if($strDataPlayer["BlackJack"] == 1){
        	        					                    $arrAllDataPlayer[$player]["SpecialEvents".$y]  = "BlackJack";
        	        					                }
        	        					            }
        	        					            
        	        					            if(array_key_exists("WinMain", $strDataPlayer)){

        	        					                $arrAllDataPlayer[$player]["WinMain"] += $strDataPlayer['WinMain'];
        	        					            }
        	        					            if(array_key_exists("WinC213", $strDataPlayer)){
        	        					                
        	        					                $arrAllDataPlayer[$player]["WinC213"] += $strDataPlayer['WinC213'];
        	        					            }
        	        					            if(array_key_exists("WinPair", $strDataPlayer)){
        	        					                
        	        					                $arrAllDataPlayer[$player]["WinPair"] += $strDataPlayer['WinPair'];
        	        					            }
        	        					            
        	        					            //Solo se Cards � diverso da quello precedente
        	        					            if(!array_key_exists("Cards".$y,$arrAllDataPlayer[$player])){
        	        					                $yOld = $y - 1;
        	        					                if(array_key_exists("Cards".$yOld,$arrAllDataPlayer[$player])){
        	        					                    if($arrAllDataPlayer[$player]["Cards".$yOld] !=  $strDataPlayer["Cards"]){
            	        					                    $arrAllDataPlayer[$player]["Cards".$y] = $strDataPlayer["Cards"];
            	        					                }
        	        					                }
        	        					                else{
        	        					                    $arrAllDataPlayer[$player]["Cards".$y] = $strDataPlayer["Cards"];
        	        					                }
        	        					            }
        	        					            //Incremento y solo se le Cards sono diverse dal precedente e se ci sono eventi speciali
        	        					            if(array_key_exists("Cards".$y,$arrAllDataPlayer[$player])){
        	        					                $y++;
        	        					            }
        	        					        }
        	        					        else{
        	        					            
        	        					            if("P0".$msgInDataToSpit[1] == $player){
        	        					                $arrAllDataPlayer["P0".$msgInDataToSpit[1]]["SpecialEvents".($y - 1)]  = "Stay";
        	        					                
        	        					            }
        	        					        }
        	        					    }
        	        					}
        	        					if($y + 1 > $nFase){
        	        					    $nFase = $y + 1;
        	        					}

        	        					$url = $rowsClient[0]['url_game']."resources/Symbol/symbolON";
        	        					$src = imagecreatefrompng($url.".png");
        	        					$stringSymbols = file_get_contents($url.".json");
        	        					$dataSymbols = json_decode($stringSymbols, true);
        	        					$img = $dataSymbols['frames'];
        	        					foreach($img as $imKey=>$imValue)
        	        					{
        	        					    
        	        					    $srcX = $imValue['frame']['x'];
        	        					    $srcY = $imValue['frame']['y'];
        	        					    $srcWidth = $imValue['frame']['w'];
        	        					    $srcHeight = $imValue['frame']['h'];
        	        					    
        	        					    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
        	        					    imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
        	        					    
        	        					    $arrSymbolImage[$imKey] = $dest;
        	        					    
        	        					}
        	        					?>
        	        				<table id="session-detail-table" class="table table-lg">
        	        					<thead>
        	        					<tr>
                                	        <th>Postazioni di gioco</th>
                                	        <?php 
                                	        for($i = 1; $i <= $nFase; $i++){                                	                
                                	            ?>
                                	            <th>Fase <?=$i?></th>
                                	            <?php 
                                	        }
                                	        
                                	        ?>
                                	        <th>Vincita Main</th>
                                	        <th>Vincita 21x3</th>
                                	        <th>Vincita Coppia</th>
        	        					</tr>
        	        					</thead>
        	        					<tbody>
        	        					<?php 
        	        					//Visualizzo i dati
        	        					foreach($arrAllDataPlayer as $key=>$value){
        	        					    $y= 0;     
        	        					?>
            	        					    <tr>
            	        					    	<td><?=$key?></td>
            	        					    	<?php 
            	        					    	for($i = 0; $i< $nFase; $i++){
        	        					    	        $element = null;
        	        					    	        $elementSplit = null;
        	        					    	        $elementPoint = null;
        	        					    	        $elementSpecialEvent = null;
        	        					    	        if(array_key_exists("Split".$y, $value)){
        	        					    	            $elementSplit = $value["Split".$y]["Cards"];
        	        					    	            $elementPoint = $value["Split".$y]["Point"];
        	        					    	            //Eventi speciali nello split
        	        					    	            if(array_key_exists("Break", $value["Split".$y])){
        	        					    	                if($value["Split".$y]["Break"] == 1){
        	        					    	                    $elementSpecialEvent  = "Over";
        	        					    	                }
        	        					    	            }
        	        					    	            
        	        					    	            if(array_key_exists("BlackJack", $value["Split".$y])){
        	        					    	                if($value["Split".$y]["BlackJack"] == 1){
        	        					    	                    $elementSpecialEvent  = "BlackJack";
        	        					    	                }
        	        					    	            }
        	        					    	            
        	        					    	            if(array_key_exists("Stay", $value["Split".$y])){
        	        					    	                $elementSpecialEvent  = "Stay";
        	        					    	                
        	        					    	            }
        	        					    	            
        	        					    	        }
        	        					    	        if(array_key_exists("Cards".$y, $value)){
        	        					    	            //Elimino la carta nello split
        	        					    	            if($elementSplit != null){
        	        					    	                if (($key = array_search($elementSplit[0], $value["Cards".$y])) !== false) {
        	        					    	                    echo $key;
        	        					    	                    array_splice($value["Cards".$y], $key, 1);
        	        					    	                }
        	        					    	            }
        	        					    	            $element = $value["Cards".$y];
        	        					    	        }
    	        					    	            if($element != null){
         	        					    	  ?>
                                            				<td>
                                    				<?php 
                                    				    $combination = $element;
                                    				    $image = getImageCombination($rowsClient[0],$combination, 50,65, $arrSymbolImage);
                                    				    
                                    				    $className = "col-md-12";
                                    				    if($elementSplit != null){
                                    				        $className = "col-md-6";
                                    				    }
    	                                            ?>
                                                        	    <div class="row">
                                                                	<div class=<?=$className?>>
                                                                    	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                                    </div>
                                                                    <?php 
                                                                    if($elementSplit != null){
                                                                    
                                    				                    $imageSplit = getImageCombination($rowsClient[0],$elementSplit, 50,65, $arrSymbolImage);
                                    				                ?>
                                    				    
                                                                    <div class="col-md-6">
                                                                    	<img src="data:image/png;base64,<?=$imageSplit?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                                    </div>
                                                                    <?php 
                                                                    }
                                                                    ?>
                                                        		</div>
                                                        		
                                                        		<div class="row">
                                                        			<div class=<?=$className?> style="height:15px;">
                                                        			Punti : 
                                                        		<?php 
                                                        		if(array_key_exists("Point".$y, $value)){
                                                        		?>
                                                        		
                                                        			<?=$value["Point".$y]?>
                                                        			
                                                        		<?php 
                                                        		}
                                                        		?>
                                                        			</div>
                                                        			<?php 
                                                        			if($elementPoint != null){
                                                        			    ?>
                                                        			<div class="col-md-6">
                                                        			
                                                        			
                                                        			Punti : 
                                                        			    <?=$elementPoint;?>
                                                        			
                                                        			</div>
                                                        			<?php 
                                                        			}
                                                        			?>
                                                        		</div>
                                                        		<div class="row">
                                                        			<div class=<?=$className?> style="height:10px;">
                                                        		<?php 
                                                        		if(array_key_exists("SpecialEvents".$y, $value)){
                                                        		?>
                                                        		
                                                        			<?=$value["SpecialEvents".$y]?>
                                                        			
                                                        		<?php 
                                                        		}
                                                        		?>
                                                        			</div>
                                                        			<?php 
                                                        			if($elementSpecialEvent != null){
                                                        			    ?>
                                                        			<div class="col-md-6">
                                                        			    <?=$elementSpecialEvent;?>
                                                        			
                                                        			</div>
                                                        			<?php 
                                                        			}
                                                        			?>
                                                        		</div>
                                            				</td>
                                            			<?php 
                                            			
        	        					    	            }
        	        					    	            else{
        	        					    	                ?>
                                                			    <td>-</td>
                                                			    <?php 
                                            			    }
            	        					    	        $y++;
            	        					    	}
                                        			?>
                                        			<td><?=$value["WinMain"]?></td>
                                        			<td><?=$value["WinC213"]?></td>
                                        			<td><?=$value["WinPair"]?></td>
                                        		</tr>
            	        					    <?php 
        	        					}
        	        					?>
        	        					</tbody>
        	        				</table>
        	        			</div>
        	        		</div>
        	        	</div>
        	        </div>
        	        <div class="col-sm-12 col-md-12">
        	        	<div class="box">
    	        			<div class="box-body">
        	        			<div class="table-responsive">
        	        				<table id="session-detail-table" class="table table-lg">
        	        					<thead>
        	        					<tr>
                                	        <th>Transazione</th>
                                	        <th>Bet Main</th>
                                	        <th>Bet 21x3</th>
                                	        <th>Bet Coppia</th>
                                	        <th>Vincita Main</th>
                                	        <th>Vincita 21x3</th>
                                	        <th>Vincita Coppia</th>
        	        					</tr>
        	        					</thead>
        	        					<tbody>
        	        <?php
                            	        $transaction = "";
                            	        $index = 0;
                            	        
                            	        $betMain = 0;
                            	        $bet213 = 0;
                            	        $betCoppia = 0;
                            	        
                            	        $vincitaMain = 0;
                            	        $vincita213 = 0;
                            	        $vincitaCoppia = 0;
                            	        
                            	        foreach($rowsClient as $row){

                            	            if($row['msg_out_client'] != null){
                            	                
                            	                $msgToSpit = explode(";", $row['msg_out_client']);
                            	                $msgOutDataToSpit = explode("=", $msgToSpit[1]);
                            	                $msgOutClient = json_decode($msgOutDataToSpit[1], true);

                            	                //Recupero il bet e le vincite
                            	                if($index == count($rowsClient) - 1){
                            	                    $transaction .= $row['id_transaction_client'];
                            	                    foreach($msgOutClient as $player){
                            	                        if(is_array($player)){
                            	                            
                            	                            if(array_key_exists("Main", $player)){
                            	                                if($player['Main'] != $betMain){
                            	                                    $betMain += $player['Main'];
                            	                                }
                            	                            }
                            	                            if(array_key_exists("C213", $player)){
                            	                                if($player['C213'] != $bet213){
                            	                                    $bet213 += $player['C213'];
                            	                                }
                            	                            }
                            	                            if(array_key_exists("Pair", $player)){
                            	                                if($player['Pair'] != $betCoppia){
                            	                                    $betCoppia += $player['Pair'];
                            	                                }
                            	                            }
                            	                            
                            	                            if(array_key_exists("WinMain", $player)){
                            	                                $vincitaMain += $player['WinMain'];
                            	                            }
                            	                            if(array_key_exists("WinC213", $player)){
                            	                                $vincita213 += $player['WinC213'];
                            	                            }
                            	                            if(array_key_exists("WinPair", $player)){
                            	                                $vincitaCoppia += $player['WinPair'];
                            	                            }
                            	                        }
                            	                    }
                            	                }  
                            	            }
                            	            
                            	            $index++;
                            	        }
        	        ?>
                                        <tr>
                                        	<td><?=$transaction?></td>
                                            <td><?=$betMain?></td>
                                            <td><?=$bet213?></td>
                                            <td><?=$betCoppia?></td>
                                            <td><?=$vincitaMain?></td>
                                            <td><?=$vincita213?></td>
                                            <td><?=$vincitaCoppia?></td>
                                        </tr>
                                    </tbody>
                            	</table>                                
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php
        	        break;
        	    case TABLE_GAME_PLATFORM_OLD:
        	        ?>
        	        <div class="row">
        	        	<div class="col-12">
        	        		<div class="box">
        	        			<div class="box-body">
        	        				<div style="display: block;height: auto;">
        	       	<?php
        	       	
        	       	    //Recupero le immagini del gioco
            	       	$url = $rowsClient[0]['url_game']."resources/Symbol/symbolON";
            	       	$src = imagecreatefrompng($url.".png");
            	       	$stringSymbols = file_get_contents($url.".json");
            	       	$dataSymbols = json_decode($stringSymbols, true);
            	       	$img = $dataSymbols['frames'];
            	       	
            	       	//$par = null;
            	       	
            	       	
            	       	
                        for($i = 0; $i< sizeof($rows); $i++){
                            $row = $rows[$i];
                            $msgOutSSS = json_decode($row['msg_out_sss']);
                            if($i == 0){
        	                    
                                foreach($rowsClient as $rowClient){
                                    $msgInClient = explode(";",$rowClient['msg_in_client']);
                                    $parEl = explode("=",$msgInClient[count($msgInClient) - 2]);
                                    
                                    $parValue = $parEl[1];
                                    
                                    if($parValue != ""){
                                        $par = explode(",", $parValue);
                                        
                                    }
                                }
                                
        	                    
                            }
        	                else{
        	                    $par = null;
        	                    
        	                }
        	                
        	                foreach($img as $imKey=>$imValue)
        	                {
        	                    
        	                    $srcX = $imValue['frame']['x'];
        	                    $srcY = $imValue['frame']['y'];
        	                    $srcWidth = $imValue['frame']['w'];
        	                    $srcHeight = $imValue['frame']['h'];
        	                    
        	                    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
        	                    imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
        	                    
        	                    $arrSymbolImage[$imKey] = $dest;
        	                    
        	                }
        	                
        	                $combination = $msgOutSSS->config;
        	                $image = getImageCombination($row,$combination, 200,254, $arrSymbolImage, $par);
        	        ?>
                                	    <div class="row">
                                        	<div class="col-md-12">
                                            	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                            </div>
                                		</div>
                    <?php 
                	}
                    ?>
                    				</div>
                                </div>
                            </div>
                        </div>
                	</div>
                	<div class="col-sm-12 col-md-12">
                	<div class="box">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="session-detail-table" class="table table-lg">
                                    <thead>
                                        <tr>
                                            <th>Transazione</th>
                                            <th>Bet</th>
                                            <th>Vincita Totale</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                <?php
                                $transaction = "";
                                foreach($rows as $row){
                                    
                                    $msgOutSSS = json_decode($row['msg_out_sss']);
                                    
                                    $transaction .= $row['id_transaction']." - ";
                                    $vincitaTotale = 0;
                                    
                                    if($msgOutSSS->VincitaTotale == 0){
                                        $msgInClient = explode(";",$row['msg_in_client']);
                                        $betArr = explode("=",$msgInClient[1]);
                                        $bet = $betArr[1];
                                    }
                                    else{
                                        
                                        $vincitaTotale = $msgOutSSS->VincitaTotale;
                                    }
                                    
                                }
                                $transaction = substr($transaction, 0, strlen($transaction) - 3);
                                    ?>
                                        <tr>
                                        	<td><?=$transaction?></td>
                                            <td><?=$bet?></td>
                                            <td><?=$vincitaTotale?></td>
                                        </tr>
                                    </tbody>
                            	</table>                                
                            </div>
                        </div>
                    </div>
                	<?php 
        	        break;
        	    default:
        	        
        	        switch($rowsClient[0]['id_game']){
        	            
        	            case "210":
        	                /**
        	                 * Gioco MysteryCastle
        	                 */
        	                $font = $rowsClient[0]['url_game']."resources/Font/FontKeyboard.ttf";
        	                
        	                $backgroundImage = imagecreatefrompng($rowsClient[0]['url_game']."resources/MainGame/MainTable.png");
        	                
        	                //Recupero immagini porta sopra
        	                $urlUpDoorImage = $rowsClient[0]['url_game']."resources/Symbol/MainDoor";
        	                $srcUpDoor = imagecreatefrompng($urlUpDoorImage.".png");
        	                imagesavealpha($srcUpDoor, true);
        	                $stringUpDoor = file_get_contents($urlUpDoorImage.".json");
        	                $dataUpDoor = json_decode($stringUpDoor, true);
        	                
        	                $srcUpDoorX = $dataUpDoor['frames']["woodDoorAnimated4.png"]['frame']['x'];
        	                $srcUpDoorY = $dataUpDoor['frames']["woodDoorAnimated4.png"]['frame']['y'];
        	                $srcUpDoorWidth = $dataUpDoor['frames']["woodDoorAnimated4.png"]['frame']['w'];
        	                $srcUpDoorHeight = $dataUpDoor['frames']["woodDoorAnimated4.png"]['frame']['h'];
        	                
        	                $destUpDoor = imagecreatetruecolor($srcUpDoorWidth, $srcUpDoorHeight);
        	                imagefill($destUpDoor,0,0,0x7fff0000);
        	                imagecopy($destUpDoor, $srcUpDoor, 0, 0, $srcUpDoorX, $srcUpDoorY, $srcUpDoorWidth, $srcUpDoorHeight);
        	                
        	                
        	                //Recupero immagini porta sotto
        	                $urlSubDoorImage = $rowsClient[0]['url_game']."resources/Symbol/SubDoor";
        	                $srcSubDoor = imagecreatefrompng($urlSubDoorImage.".png");
        	                $stringSubDoor = file_get_contents($urlSubDoorImage.".json");
        	                $dataSubDoor = json_decode($stringSubDoor, true);
        	                
        	                $srcSubDoorX = $dataSubDoor['frames']["portCullisAnimated7.png"]['frame']['x'];
        	                $srcSubDoorY = $dataSubDoor['frames']["portCullisAnimated7.png"]['frame']['y'];
        	                $srcSubDoorWidth = $dataSubDoor['frames']["portCullisAnimated7.png"]['frame']['w'];
        	                $srcSubDoorHeight = $dataSubDoor['frames']["portCullisAnimated7.png"]['frame']['h'];
        	                
        	                $destSubDoor = imagecreatetruecolor($srcSubDoorWidth, $srcSubDoorHeight);
        	                imagefill($destSubDoor,0,0,0x7fff0000);
        	                imagecopy($destSubDoor, $srcSubDoor, 0, 0, $srcSubDoorX, $srcSubDoorY, $srcSubDoorWidth, $srcSubDoorHeight);
        	                
        	                
        	                //Recupero immagine simboli
        	                $url = $rowsClient[0]['url_game']."resources/Symbol/symbolONdown";
        	                
        	                $src = imagecreatefrompng($url.".png");
        	                $stringSymbols = file_get_contents($url.".json");
        	                $dataSymbols = json_decode($stringSymbols, true);
        	                $nImage = count($dataSymbols['frames']);
        	                
        	                $arrSymbolImage = [];
        	                
        	                /**
        	                 * Ottengo un'array contenente i simboli del gioco
        	                 */
        	                $start = 0;
        	                $end = $nImage - 1;
        	                
        	                for($i = $start; $i <= $end; $i++)
        	                {
        	                    $index = str_pad($i, 2, "0", STR_PAD_LEFT);
        	                    $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
        	                    $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
        	                    $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
        	                    $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
        	                    
        	                    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
        	                    imagefill($dest,0,0,0x7fff0000);
        	                    imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
        	                    array_push($arrSymbolImage, $dest);
        	                    
        	                }
        	                ?>
        	                <div class="row">
        	                	<div class="col-12">
        	                		<div class="box">
        	                			<div class="box-body">
        	                				<div style="display: block;height: auto;">
        	                <?php
        	                
        	                $combination = $msgOutSSS->Combination;
        	                $image = getImageCombinationMystery($row,$combination, 148, 148, $arrSymbolImage, $backgroundImage, $destUpDoor, $destSubDoor, $font);
        	               ?>
        	               						<div class="row">
            	               						<div class="col-md-2">
            	               						</div>
                                                	<div class="col-md-8">
                                                    	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
            
                                                    </div>
                                                    <div class="col-md-2">
            	               						</div>
                                                </div>
        	               					</div>
                                    	</div>
                    				</div>
                    			</div>
                    		</div>
        	               <div class="col-sm-12 col-md-12">
        	               		<div class="box">
        	               			<div class="box-body">
        	               				<div class="table-responsive">
        	               					<table id="session-detail-table" class="table table-lg">
        	               					<thead>
        	               						<tr>
                                	               <th>Transazione</th>
                                	               <th>Bet</th>
                                	               <th>Vincita Totale</th>
        	               						</tr>
        	               					</thead>
        	               					<tbody>
        	               <?php
        	               foreach($rows as $row){
        	                   $transaction = $row['id_transaction'];
        	                   
        	                   
        	                   $vincitaTotale = $msgOutSSS->VincitaTotale;
        	                   
        	                   $msgInClient = explode(";",$row['msg_in_client']);
        	                   $betArr = explode("=",$msgInClient[1]);
        	                   $bet = $betArr[1];
        	                   
        	                   ?>
                                                    <tr>
                                                    	<td><?=$transaction?></td>
                                                        <td><?=$bet?></td>
                                                        <td><?=$vincitaTotale?></td>
                                                    </tr>
                                              <?php 
                                                }
                                              ?>
                                            </tbody>
                                    	</table>
                                    </div>
                                </div>
                            </div>
                            <?php 
        	                break;
                        case "211":
                        /**
                         * Gioco FortuneRun
                         */
                        ?>
        	               <div class="col-sm-12 col-md-12">
        	               		<div class="box">
        	               			<div class="box-body">
        	               				<div class="table-responsive">
        	               					<table id="session-detail-table" class="table table-lg">
        	               					<thead>
        	               						<tr>
                                	               <th>Transazione</th>
                                	               <th>Bet</th>
                                	               <th>Moltiplicatore a fine Partita</th>
                                	               <th>Vincita Totale</th>
        	               						</tr>
        	               					</thead>
        	               					<tbody>
        	               <?php
        	                   $transaction = $row['id_transaction'];
        	                   
        	                   
        	                   $vincitaTotale = $msgOutSSS->VincitaTotale;
        	                   
        	                   $msgInClient = explode(";",$row['msg_in_client']);
        	                   $betArr = explode("=",$msgInClient[1]);
        	                   $bet = $betArr[1];
        	                   
        	                   ?>
                                                    <tr>
                                                    	<td><?=$transaction?></td>
                                                        <td><?=$bet?></td>
                                                        <td><?=$vincitaTotale/$bet?></td>
                                                        <td><?=$vincitaTotale?></td>
                                                    </tr>
                                            </tbody>
                                    	</table>
                                    </div>
                                </div>
                            </div>
                            <?php 
        	                break;
                         case "212":
                             /**
                              * Gioco KickAndWIn
                              */
                             ?>
        	               <div class="col-sm-12 col-md-12">
        	               		<div class="box">
        	               			<div class="box-body">
        	               				<div class="table-responsive">
        	               					<table id="session-detail-table" class="table table-lg">
        	               					<thead>
        	               						<tr>
                                	               <th>Transazione</th>
                                	               <th>Bet</th>
                                	               <th>Vincita Totale</th>
                                	               <th>Combinazione</th>
                                	               <th>Rigore</th>
        	               						</tr>
        	               					</thead>
        	               					<tbody>
        	               <?php
        	                   $transaction = $row['id_transaction'];
        	                   
        	                   
        	                   $vincitaTotale = $msgOutSSS->VincitaTotale;
        	                   //Recupero immagine simboli
        	                   $url = $rowsClient[0]['url_game']."resources/Symbol/symbolON";
        	                   
        	                   $src = imagecreatefrompng($url.".png");
        	                   $stringSymbols = file_get_contents($url.".json");
        	                   $dataSymbols = json_decode($stringSymbols, true);
        	                   $nImage = count($dataSymbols['frames']);
        	                   
        	                   $arrSymbolImage = [];
        	                   
        	                   /**
        	                    * Ottengo un'array contenente i simboli del gioco
        	                    */
        	                   
        	                   for($i = 1; $i <= 3; $i++)
        	                   {
        	                       $index = str_pad($i, 2, "0", STR_PAD_LEFT);
        	                       $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
        	                       $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
        	                       $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
        	                       $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
        	                       
        	                       $dest = imagecreatetruecolor($srcWidth, $srcHeight);
        	                       imagefill($dest,0,0,0x7fff0000);
        	                       imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
        	                       array_push($arrSymbolImage, $dest);
        	                       
        	                   }
        	                   $comb = $msgOutSSS->Combination->PathRun;
        	                   
        	                   $isRigore = "";
        	                   if($comb[4] == 0){
        	                       if(in_array(3, $comb)){
        	                           $isRigore = "-";
        	                       }
        	                       else{
        	                           $isRigore = "PARATA";
        	                       }
        	                   }
        	                   else{
        	                       $isRigore = "GOAL";
        	                   }
        	                   
        	                   
        	                   array_pop($comb);
        	                   $imageComb = getImageCombinationScratch($row,$comb, 41, 44, $arrSymbolImage, 1, 4);
        	                   
        	                   $msgInClient = explode(";",$row['msg_in_client']);
        	                   $betArr = explode("=",$msgInClient[1]);
        	                   $bet = $betArr[1];
        	                   
        	                   ?>
                                                    <tr>
                                                    	<td><?=$transaction?></td>
                                                        <td><?=$bet?></td>
                                                        <td><?=$vincitaTotale?></td>
                                                        <td>
                                                        	<img src="data:image/png;base64,<?=$imageComb?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                        </td>
                                                        <td><?=$isRigore?></td>
                                                    </tr>
                                            </tbody>
                                    	</table>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            break;
                         case "213":
                            /**
                             * Gioco FruitRace
                             */
                             ?>
                             <div class="col-sm-12 col-md-12">
                             	<div class="box">
                             		<div class="box-body">
                             			<div class="table-responsive">
                             				<table id="session-detail-table" class="table table-lg">
                             					<thead>
                             						<tr>
                                                     <th>Transazione</th>
                                                     <th>Bet</th>
                                                     <th>Vincita Totale</th>
                             						</tr>
                             					</thead>
                             					<tbody>
                                                 <?php
                                                 foreach($rows as $row){
                                                     $transaction = $row['id_transaction'];
                                                     
                                                     
                                                     $vincitaTotale = $msgOutSSS->VincitaTotale;
                                                     
                                                     $msgInClient = explode(";",$row['msg_in_client']);
                                                     $betArr = explode("=",$msgInClient[1]);
                                                     $bet = $betArr[1];
                                                     
                                                     ?>
                                                                        <tr>
                                                                        	<td><?=$transaction?></td>
                                                                            <td><?=$bet?></td>
                                                                            <td><?=$vincitaTotale?></td>
                                                                        </tr>
                                                                  <?php 
                                                }
                                                                  ?>
                                            	</tbody>
                                    		</table>
                                		</div>
                                	</div>
                            	</div>
                             <?php 
                             //Recupero immagine simboli
                             $url = $rowsClient[0]['url_game']."resources/Symbol/symbolOn";
                             
                             $src = imagecreatefrompng($url.".png");
                             $stringSymbols = file_get_contents($url.".json");
                             $dataSymbols = json_decode($stringSymbols, true);
                             $nImage = count($dataSymbols['frames']);
                             
                             $arrSymbolImage = [];
                             $arrSymbolImageTot = [];
                             
                             /**
                              * Ottengo un'array contenente i simboli del gioco
                              */
                             
                             for($i = 1; $i <= 6; $i++)
                             {
                                 $index = str_pad($i, 2, "0", STR_PAD_LEFT);
                                 $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
                                 $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
                                 $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
                                 $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
                                 
                                 $dest = imagecreatetruecolor($srcWidth, $srcHeight);
                                 imagefill($dest,0,0,0x7fff0000);
                                 imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
                                 array_push($arrSymbolImage, $dest);
                                 
                                 $destTot = imagecreatetruecolor($srcWidth, $srcHeight);
                                 imagefill($destTot,0,0,0x7fff0000);
                                 imagecopy($destTot, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
                                 array_push($arrSymbolImageTot, $destTot);
                                 
                             }
                             
                             
                             $arrayCombination = [];
                             $arrayTotalePunti = [0,0,0,0,0,0];
                             
                             for($i = 1; $i <= 6; $i++){
                                 $obj = new stdClass();
                                 
                                 //Aggiungo combinazione
                                 $combination = $msgOutSSS->Combination->{"MapScreenTiro0".$i};
                                 $obj->combination = $combination;
                                 
                                 //Aggiungo punti
                                 $obj->Frutta01 = $msgOutSSS->Combination->{"Frutta01_".$i};
                                 $obj->Frutta02 = $msgOutSSS->Combination->{"Frutta02_".$i};
                                 $obj->Frutta03 = $msgOutSSS->Combination->{"Frutta03_".$i};
                                 $obj->Frutta04 = $msgOutSSS->Combination->{"Frutta04_".$i};
                                 $obj->Frutta05 = $msgOutSSS->Combination->{"Frutta05_".$i};
                                 $obj->Frutta06 = $msgOutSSS->Combination->{"Frutta06_".$i};
                                 
                                 $arrayTotalePunti[0] += $obj->Frutta01;
                                 $arrayTotalePunti[1] += $obj->Frutta02;
                                 $arrayTotalePunti[2] += $obj->Frutta03;
                                 $arrayTotalePunti[3] += $obj->Frutta04;
                                 $arrayTotalePunti[4] += $obj->Frutta05;
                                 $arrayTotalePunti[5] += $obj->Frutta06;
                                 
                                 if($combination[0] == 0){
                                     continue;
                                 }
                                 array_push($arrayCombination, $obj);
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta01 == 1){
                                 $image01Png = GetSymbolImage($arrSymbolImageTot, 0, false);
                                 $arrayTotalePunti[0] = "VITTORIA";
                             }
                             else{
                                 $image01Png = GetSymbolImage($arrSymbolImageTot, 0, true);
                                 $arrayTotalePunti[0] = $arrayTotalePunti[0]."/26";
                                 
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta02 == 1){
                                 $image02Png = GetSymbolImage($arrSymbolImageTot, 1, false);
                                 $arrayTotalePunti[1] = "VITTORIA";
                             }
                             else{
                                 $image02Png = GetSymbolImage($arrSymbolImageTot, 1, true);
                                 $arrayTotalePunti[1] = $arrayTotalePunti[1]."/18";
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta03 == 1){
                                 $image03Png = GetSymbolImage($arrSymbolImageTot, 2, false);
                                 $arrayTotalePunti[2] = "VITTORIA";
                             }
                             else{
                                 $image03Png = GetSymbolImage($arrSymbolImageTot, 2, true);
                                 $arrayTotalePunti[2] = $arrayTotalePunti[2]."/20";
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta04 == 1){
                                 $image04Png = GetSymbolImage($arrSymbolImageTot, 3, false);
                                 $arrayTotalePunti[3] = "VITTORIA";
                             }
                             else{
                                 $image04Png = GetSymbolImage($arrSymbolImageTot, 3, true);
                                 $arrayTotalePunti[3] = $arrayTotalePunti[3]."/22";
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta05 == 1){
                                 $image05Png = GetSymbolImage($arrSymbolImageTot, 4, false);
                                 $arrayTotalePunti[4] = "VITTORIA";
                                 
                             }else{
                                 $image05Png = GetSymbolImage($arrSymbolImageTot, 4, true);
                                 $arrayTotalePunti[4] = $arrayTotalePunti[4]."/24";
                             }
                             
                             if($msgOutSSS->Combination->VincoFrutta06 == 1){
                                 $image06Png = GetSymbolImage($arrSymbolImageTot, 5, false);
                                 $arrayTotalePunti[5] = "VITTORIA";
                             }
                             else{
                                 $image06Png = GetSymbolImage($arrSymbolImageTot, 5, true);
                                 $arrayTotalePunti[5] = $arrayTotalePunti[5]."/28";
                             }
                             
                             
                             
                             ?>
                             <div class="col-sm-12 col-md-12">
                             	<div class="box">
                             		<div class="box-body">
                             			<div class="table-responsive" style="overflow-x: hidden;">
                             				<table id="session-detail-table" class="table table-lg">
                             					<thead>
                             						<tr>
                                                     <th>Punti totali</th>
                             						</tr>
                             					</thead>
                             					<tbody>

                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image01Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[0]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image02Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[1]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image03Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[2]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image04Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[3]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image05Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[4]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                                    <tr>
                                                    	<td>
                                                    		<div class="row">
                                                				<div class="col-md-6">
                                                					<img src="data:image/png;base64,<?=$image06Png?>" alt="Symbol Image">
                                                				</div>
                                                				<div class="col-md-6 my-auto">
                                                					<?=$arrayTotalePunti[5]?>
                                                				</div>
                                                			</div>
                                                    	</td>
                                                    </tr>
                                            	</tbody>
                                    		</table>
                                		</div>
                                	</div>
                            	</div>
                             
                             
                             <div class="box">
                             	<div class="box-body">
                             		<div class="table-responsive"  style="overflow-x: hidden;">
                             			<table id="session-detail-table" class="table table-lg">
                                             <thead>
                                                 <tr>
                                                 	<th>Combinazione</th>
                                                 	<th>Punti tiro</th>
                                                 </tr>
                                             </thead>
                                             <tbody>
                             <?php

                             $image01Png = GetSymbolImage($arrSymbolImage, 0, false);
                             $image02Png = GetSymbolImage($arrSymbolImage, 1, false);
                             $image03Png = GetSymbolImage($arrSymbolImage, 2, false);
                             $image04Png = GetSymbolImage($arrSymbolImage, 3, false);
                             $image05Png = GetSymbolImage($arrSymbolImage, 4, false);
                             $image06Png = GetSymbolImage($arrSymbolImage, 5, false);
                             
                             $i = 1;
                             
                             foreach($arrayCombination as $comb){
                                 
                                 
                                 $imageComb = getImageCombinationScratch($row,$comb->combination, 41, 44, $arrSymbolImage, 5, 5);
                                 
                                 //Aggiungere i punti per ogni frutto
                                 ?>
                                                <tr>
                                                	<td rowspan="6">
                                        				<img src="data:image/png;base64,<?=$imageComb?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                	</td>
                                                	<td>
                                                		<div class="row">
                                                			<div class="col-md-6">
                                                				<img src="data:image/png;base64,<?=$image01Png?>" alt="Symbol Image">
                                                			</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta01?>
                                                			</div>
                                                		</div>
                                                	</td>	
                                                        
                                                 </tr> 
                                                 <tr>  
                                                 	<td>
                                                 		<div class="row">
                                                     		<div class="col-md-6">
                                                     			<img src="data:image/png;base64,<?=$image02Png?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                     		</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta02?>
                                                			</div>
                                                		</div>
                                                 	</td>	
                                                 </tr>
                                                 <tr>  
                                                 	<td>
                                                 		<div class="row">
                                                     		<div class="col-md-6">
                                                 				<img src="data:image/png;base64,<?=$image03Png?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                 			</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta03?>
                                                			</div>
                                                		</div>
                                                 	</td>	
                                                 </tr>
                                                 <tr>  
                                                 	<td>
                                                 		<div class="row">
                                                     		<div class="col-md-6">
                                                 				<img src="data:image/png;base64,<?=$image04Png?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                 			</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta04?>
                                                			</div>
                                                		</div>
                                                 	</td>	
                                                 </tr>
                                                 <tr>  
                                                 	<td>
                                                 		<div class="row">
                                                     		<div class="col-md-6">
                                                 				<img src="data:image/png;base64,<?=$image05Png?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                 			</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta05?>
                                                			</div>
                                                		</div>
                                                 	</td>	
                                                 </tr>
                                                 <tr>  
                                                 	<td>
                                                 		<div class="row">
                                                     		<div class="col-md-6">
                                                 				<img src="data:image/png;base64,<?=$image06Png?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                 			</div>
                                                			<div class="col-md-6 my-auto">
                                                				<?=$comb->Frutta06?>
                                                			</div>
                                                		</div>
                                                 	</td>	
                                                 </tr>

                                            <?php 
                                            $i++;
                            }
                                            ?>
                                        </tbody>
                            		</table>
                         		</div>
                			</div>
                 		</div>	
        	               <?php 
        	                break;
        	            default:
        	                //Slot
        	                $url = $rowsClient[0]['url_game']."resources/Symbol/symbolON";
        	                
        	                $src = imagecreatefrompng($url.".png");
        	                $stringSymbols = file_get_contents($url.".json");
        	                $dataSymbols = json_decode($stringSymbols, true);
        	                $nImage = count($dataSymbols['frames']);
        	                
        	                $arrSymbolImage = [];
        	                
        	                /**
        	                 * Ottengo un'array contenente i simboli del gioco
        	                 */
        	                $start = 1;
        	                $end = $nImage;
        	                
        	                for($i = $start; $i <= $end; $i++)
        	                {
        	                    $index = str_pad($i, 2, "0", STR_PAD_LEFT);
        	                    $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
        	                    $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
        	                    $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
        	                    $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
        	                    
        	                    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
        	                    imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
        	                    array_push($arrSymbolImage, $dest);
        	                    
        	                }
        	                ?>
        	                <div class="row">
                                <div class="col-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div style="display: block;height: auto;">
                                            	<?php   
                                            	        
                                                	    $combination = $msgOutSSS->Combination;

                                            	        $image = getImageCombination($row,$combination, 128,128, $arrSymbolImage);
                                                	    
                                                	    
                                                	    ?>
                                                	    <div class="row">
                                                	<div class="col-md-8">
                                                    	<img src="data:image/png;base64,<?=$image?>" id="imageConf" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
            
                                                    </div>
                                                    <div class="col-md-4">
                                                    	<table id="session-detail-table" class="table table-lg">
                                                            <thead>
                                                                <tr>
                                                                	<th>Linea vincente</th>
                                                                	<th>Simboli vincenti sulla linea</th>
                                                                    <th>Simbolo vincente</th>
                                                                </tr>
                                                            </thead>
                                                			<tbody>
                                                    		<?php
                                                    		
                                                    		foreach($lineeVincita as $lv){
                                                    		    $linee = $lv->p;
                                                    		    $strLinee = "";
                                                    		    foreach($linee as $l){
                                                    		        $pos = intval($l)+1;
                                                    		        $strLinee .= $pos.",";
                                                    		    }
                                                    		    $strLinee = substr($strLinee, 0, strlen($strLinee)-1);
                                                    		    $simbol = $lv->s;
                                                    		    $nLineaVincita = $lv->n;
                                                            ?>
                                                                <tr>
                                                                	<td><?=$nLineaVincita?></td>
                                                                	<td onclick='SeeWinLine(<?php echo json_encode($row["url_game"]); ?>,<?php echo json_encode($row["rows"]); ?>, <?php echo json_encode($row["columns"]); ?>,<?php echo json_encode($linee);?>, <?php  echo json_encode($combination);?>)'><?=$strLinee?></td>
                                                                    <td><?=$simbol?></td>
                                                                </tr>
                                                    		<?php 
                                                    		}
                                                    		?>
                                                			</tbody>
                                        				</table>
            
                                                	</div>
                                                	
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                    			</div>
                    			<div class="col-sm-12 col-md-12">
                        	<div class="box">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="session-detail-table" class="table table-lg">
                                            <thead>
                                                <tr>
                                                	<th>Transazione</th>
                                                    <th>Bet</th>
                                                    <th>Vincita Totale</th>
                                                    <th>Vincita Linee</th>
                                                    <th>Vincita Freespin</th>
                                                    <th>Vincita Bonus</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php   
                                                foreach($rows as $row){
                                                    $transaction = $row['id_transaction'];
                                                    
                                                    
                                                    $vincitaTotale = $msgOutSSS->VincitaTotale;
                                                    $vincitaLinee = $msgOutSSS->VincitaLinee;
                                                    $vincitaScatter = $msgOutSSS->VincitaScatter;
                                                    $vincitaBonus = $msgOutSSS->VincitaBonus;
                                                    
                                                    $msgInClient = explode(";",$row['msg_in_client']);
                                                    $betArr = explode("=",$msgInClient[1]);
                                                    $bet = $betArr[1];
        
                                                ?>
                                                    <tr>
                                                    	<td><?=$transaction?></td>
                                                        <td><?=$bet?></td>
                                                        <td><?=$vincitaTotale?></td>
                                                        <td><?=$vincitaLinee?></td>
                                                        <td><?=$vincitaScatter?></td>
                                                        <td><?=$vincitaBonus?></td>
                                                    </tr>
                                              <?php 
                                                }
                                              ?>
                                            </tbody>
                                    	</table>
                                    </div>
                                </div>
                            </div>
                    <?php 
        	        break;
}
        	?>
        		
                    
                	<?php 
                	if(isset($msgOutSSS->BonusArray)){
                	?>
                       <div class="box">
                            <div class="box-body">
                                <div class="table-responsive">
                            		<table id="session-detail-table" class="table table-lg">
                                        <thead>
                                            <tr>
                                            	<th>Vincita bonus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php   
                                            
                                            foreach($msgOutSSS->BonusArray as $bp){
                                                
                                                $win = $bp;
                                            ?>
                                                <tr>
                                                	<td><?=$win?></td>
                                                </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                            		</table>
                         		</div>
                			</div>
                 		</div>	
                	<?php 
                	    
                	}
                	?>
                     
                    <?php 
                	if(isset($msgOutSSS->FreeSpin)){
                	?>
                       <div class="box">
                            <div class="box-body">
                                <div class="table-responsive">
                            		<table id="session-detail-table" class="table table-lg">
                                        <thead>
                                            <tr>
                                            	<th>Combinazione freespin</th>
                                            	<th>Vincita totale freespin</th>
                                                <th>Linee vincita freespin</th>
                                                <th>Vincita linee freespin</th>
                                                <th>Simbolo linee freespin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php   
                                            
                                            foreach($msgOutSSS->FreeSpin as $fs){
                                                
                                                $win = $fs->Win;
                                                $combination = $fs->Combination;
                                                $imageFs = getImageCombination($row,$combination, 64,64, $arrSymbolImage);
                                                
                                                $winLines = $fs->WinLines;
                                                $rowSpan = count($winLines);
                                                if($rowSpan == 0){
                                                    $rowSpan = 1;
                                                }
                                                
                                            ?>
                                                <tr>
                                                	<td rowspan="<?=$rowSpan?>">
                                                		<img src="data:image/png;base64,<?=$imageFs?>" style="display: block;margin-left: auto;margin-right: auto" alt="Symbol Image">
                                                	</td>
                                                	<td rowspan="<?=$rowSpan?>"><?=$win?></td>	
                                                        <?php 
                                                        if($win > 0){
                                                            $i= 0;
                                                            foreach($winLines as $lines){
                                                                $linee = $lines->p;
                                                                $strLinee = "";
                                                                foreach($linee as $l){
                                                                    $pos = intval($l)+1;
                                                                    $strLinee .= $pos.",";
                                                                }
                                                                $strLinee = substr($strLinee, 0, strlen($strLinee)-1);
                                                                
                                                                $winLinee = $lines->w;
                                                                $sLinee = $lines->s;
                                                                if($i > 0){
                                                                    ?>
                                                             	<tr>  
                                                            <?php 
                                                              
                                                            }
                                                            ?>
                                                            	<td><?=$strLinee?></td>
                                                            	<td><?=$winLinee?></td>
                                                            	<td><?=$sLinee?></td>
                                                       		<?php 
                                                       	    if($i > 0){
                                                       	    ?>
                                                             </tr>  
                                                            <?php 
                                                              
                                                            }
                                                            $i++;
                                                        }
                                                      }
                                                      else{
                                                          ?>
                                                        	<td>-</td>
                                                        	<td>-</td>
                                                        	<td>-</td>
                                                        <?php 
                                                      }
                                                       	?>
                                                 </tr>   

                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                            		</table>
                         		</div>
                			</div>
                 		</div>	
                	<?php 
                	    
                	}
        	                
        	                break;
        	        }
        	        ?>

                    	
                </div>
             </div>    
    	</div>
    	</div>
    </body>
</html>