<?php
$rows = $_GET['rows'];
$columns = $_GET['columns'];
$urlGame = $_GET['url'];
$jsonCombination = $_GET['combination'];
$line = $_GET['line'];


/**
 * Recupero delle immagini dei simboli del gioco
 */
$url = $urlGame."resources/Symbol/symbolON";
$stringSymbols = file_get_contents($url.".json");
$dataSymbols = json_decode($stringSymbols, true);

$urlOFF = $urlGame."resources/Symbol/symbolOFF";
$stringSymbolsOFF = file_get_contents($urlOFF.".json");
$dataSymbolsOFF = json_decode($stringSymbolsOFF, true);


$src = imagecreatefrompng($url.".png");

$srcOFF = imagecreatefrompng($urlOFF.".png");

$nImage = count($dataSymbols['frames']);

$nImageOFF = count($dataSymbolsOFF['frames']);

$arrSymbolImage = [];
$arrSymbolImageOFF = [];

/**
 * Ottengo un'array contenente i simboli del gioco
 */
for($i = 1; $i <= $nImage; $i++)
{
    $index = str_pad($i, 2, "0", STR_PAD_LEFT);
    
    $srcX = $dataSymbols['frames'][$index.".png"]['frame']['x'];
    $srcY = $dataSymbols['frames'][$index.".png"]['frame']['y'];
    $srcWidth = $dataSymbols['frames'][$index.".png"]['frame']['w'];
    $srcHeight = $dataSymbols['frames'][$index.".png"]['frame']['h'];
    
    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
    
    imagecopy($dest, $src, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
    array_push($arrSymbolImage, $dest);
    
}

for($i = 1; $i <= $nImageOFF; $i++)
{
    $index = str_pad($i, 2, "0", STR_PAD_LEFT);
    
    $srcX = $dataSymbolsOFF['frames'][$index.".png"]['frame']['x'];
    $srcY = $dataSymbolsOFF['frames'][$index.".png"]['frame']['y'];
    $srcWidth = $dataSymbolsOFF['frames'][$index.".png"]['frame']['w'];
    $srcHeight = $dataSymbolsOFF['frames'][$index.".png"]['frame']['h'];
    
    $dest = imagecreatetruecolor($srcWidth, $srcHeight);
    
    imagecopy($dest, $srcOFF, 0, 0, $srcX, $srcY, $srcWidth, $srcHeight);
    array_push($arrSymbolImageOFF, $dest);
    
}

$arrSymbols = [];
$nColumn = 0;
$nRow = 0;
/**
 * Creo l'immagine vuota con altezza e larghezza che servono
 */
$eventSymbolImage = imagecreatetruecolor(128 * $columns, 128 * $rows);

/**
 * Ciclo sui simboli della combinazione
 */
for($i = 0; $i < count($jsonCombination); $i++)
{
    $symbol = $jsonCombination[$i];
    /**
     * Recupero l'immagine relativa al simbolo
     */
    $imageSymbol = null;
    $isWinLine = false;
    for($j = 0; $j < count($line); $j++){
        $pos = $line[$j];
        if($i == $pos){
            $isWinLine = true;
            break;
            
        }
    }
    
    if($isWinLine == true){
        $imageSymbol = $arrSymbolImage[$symbol-1];
    }
    else{
        $imageSymbol = $arrSymbolImageOFF[$symbol-1];
    }

    $imageSymbolWidth = imagesx($imageSymbol);
    $imageSymbolHeight = imagesy($imageSymbol);
    /**
     * Calcolo la colonna e la riga del simbolo
     */
    if($i != 0)
    {
        if($i % $columns == 0)
        {
            $nColumn = 0;
            $nRow++;
        }
        else{
            
            $nColumn++;
        }
    }
    /**
     * Copio l'immagine nella posizione calcolata
     */
    imagecopyresized($eventSymbolImage, $imageSymbol, 128 * $nColumn, 128 * $nRow, 0, 0, 128, 128, $imageSymbolWidth, $imageSymbolHeight);
    array_push($arrSymbols, 1);
    
    
}

ob_start();
imagepng($eventSymbolImage);
$contents = ob_get_contents();
ob_end_clean();


echo base64_encode($contents);